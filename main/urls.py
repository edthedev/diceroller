from django.conf.urls import patterns, include, url

# My TastyPie API
from dice_roller.models import RollResource

# from django.contrib import admin
# admin.autodiscover()

roll_resource = RollResource()

urlpatterns = patterns('',
    # Examples:
    url(r'api/', include(roll_resource.urls)),
    # url(r'^$', 'main.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^admin/', include(admin.site.urls)),
)

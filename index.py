__author__ = 'Edward Delaporte <edthedev@gmail.com>'
__copyright__ = 'Copyright 2012 Edward Delaporte'
__license__ = "Feel free to use, modify and share; but please don't sell. Please keep this license and my name attached in some form."
'''
A shared online dice roller to support gamers at separate computers.
Features:
  Common dice sizes.
  Keeps a log of all rolls by IP.
  Allows replacement of IP with a player name, for convenience.

Requirements:
  Python
  The bottle.py web framework
  Write access to /tmp

'''
import bottle
import random
import datetime
import os, sys
import time

from bottle import route, error, request, redirect
from string import Template

bottle.debug(True)

sys.path.insert(0, '/home/vagrant/source')
# os.chdir(os.path.dirname(__file__))

# Missing Django reverse method now. 
# Need to learn the Bottle equivalent.

# Original testing..
ROOT_URL = 'http://minecraft.edthedev.com/scripts/dice'
MEDIA_URL='http://minecraft.edthedev.com/bottle'

# Localhost testing.
ROOT_URL = 'http://127.0.0.1'
MEDIA_URL='http://127.0.0.1/static'

# Public serving
ROOT_URL = 'http://delaporte.us/dice'
MEDIA_URL = 'http://delaporte.us/dice/static'

# Files...
ROLL_LOG = '/tmp/roller.log'
HISTORY_FILE = '/tmp/roll_history.log'
NAME_FILE = '/tmp/names.ini'
LAST_ROLL = '/tmp/lastroll.html'

# Important names
GAME_MASTER = 'Game Master'

def getDieImage(sides):
  localimage = '/var/www/bottle/d%d.svg' % sides
  image = 'd%d.png' % sides

  if os.path.exists(localimage):
    return "<img src='{0}' alt='{1} sided die.' class=dice>".format(sides, sides)
  else:
    return 'IMAGE MISSING'

  # try:
  #   f = open('/var/www/bottle/d%d.svg' % sides)
  #   html = f.readlines()
  #   f.close()
  #   return ' '.join(html)
  # except:
  #   return ''

def get_user_id():
  return "{ip}.{agent}".format(
	ip=request['REMOTE_ADDR'],
	agent=request.environ.get('HTTP_USER_AGENT'),
	)

@route('/say', method='POST')
def say():
  message = request.forms.message
  name = get_name()
  logMessage(name + ' says ' + message)
  redirect(ROOT_URL)

@route('/admin', method='POST')
def doAdmin():
  # ip = request['REMOTE_ADDR']
  name = get_name()
  if name != GAME_MASTER:
    return 'Only the Game Master can do that.'
  action = request.forms.action
  redirect(ROOT_URL)

@route('/admin')
def admin():
  return '''

  '''

@error(404)
def error404(error):
    logMessage("A user visited an invalid URL: " + str(error))
    return record()
#     return '''Stop that...it's silly.'''

def getAdmin():
  name = get_name()
  html = ''
  if name == GAME_MASTER:
    html +=  '''<a href='{url}/clear'>Clear Log</a>
  '''.format(url=ROOT_URL)
  return html

def getNav(): 
  name = get_name()
  dice = [20, 12, 10, 8, 6, 4, 2]
  html = ''
  html += "<form class=nav method=POST action='{url}'>".format(url=ROOT_URL)
  for die in dice:
    # image = getDieImage(die)
    html += """
    <li class=nav>
    <button type=submit name=sides value={die} class=dice> 
    <img src='{media}/d{die}.png' class=dice>
    </button>
   </li>""".format(die=die, media=MEDIA_URL)
  if name == GAME_MASTER:
    html += '<label for=private>Private Roll</label><input id=private type=checkbox name=private />'
  html += '</form>'
  return html

@route('/setname')
def nameForm():
  return """
  <form action='' method='POST'>
  Name: <input name='name'><input type=submit> 
  </form>
  """

@route('/history')
def history():
  return record(history=True)

def logMessage(message):
  logfiles = [ROLL_LOG, HISTORY_FILE]

  for logfile in logfiles:
    # Wait for a lock 
    lockfile = logfile + '.lock' 
    while(os.path.exists(lockfile)):
      time.sleep(50)
    # Set Lock
    open(lockfile, 'w').close()

# Write to the log file.
    f = open(logfile, 'a')
    f.write(message)
    # f.write(' at ' + str(datetime.datetime.now().strftime('%H:%M:%S')))
    f.write('\n')
    f.close()

# Release lock
  # This isn't a perfect solution, but it's fine for our purposes.
    os.remove(lockfile)

@route('/clear')
def clear():
  user_id = get_user_id()
  name = get_name()
  if name == GAME_MASTER:
    os.remove(ROLL_LOG)
    logMessage("{gm} (user) cleared the log. <a href='{url}/history'>History</a>.".format(gm=GAME_MASTER, url=ROOT_URL, user=user_id))
  else:
    logMessage("Only {gm} can clear the log, {name}.".format(gm=GAME_MASTER, name=name))
#   return record()
  redirect(ROOT_URL)

def readFile(filename):
  if not os.path.exists(filename):
    open(filename, 'w').close()
  f = open(filename, 'r')
  lines = f.readlines()
  f.close()
  return lines

@route('/setname', method='POST')
def setName():
  ip = get_user_id()
  name = request.forms.name
  name = name.replace('\n', '')
  
# Read current file
  lines = readFile(NAME_FILE)

# Write back current file
  f = open(NAME_FILE, 'w')
  for line in lines:
    if not ip in line:
      f.write(line)
      f.write('\n')

# Write in new name.

  f.write(ip + name)
  f.close()
  
  logMessage('<em>' + ip + ' became ' + name + '</em>')
  redirect(ROOT_URL)

def get_name():
  ip = get_user_id()
  noname = "<a href='{url}/setname'>{ip}</a>".format(url=ROOT_URL, ip=ip)
  lines = readFile(NAME_FILE)
  for line in lines:
    if ip in line:
      return line.replace(ip, '').replace('\n', '')
  return noname

@route('/', method='POST')
def actions():
# Do the roll...
  sides = request.forms.sides
  private = request.forms.private 
  random.seed()
  message = ''
  try:
    sides = int(sides)
  except:
    message = "Error {sides}".format(sides=sides)
    sides = 2
    
  options = range(1, sides+1)
  roll = random.choice(options)

# Log the roll to the record.
  name = get_name()
  message = name + ' rolled ' + str(roll) + ' on a ' + str(sides) + ' sided die.'
  if private == 'on':
    message = name + ' privately rolled a ' + str(sides) + ' sided die.'
  
  html = ''

# Draw last roll to last roll file.
  # html += getDieImage(sides)
  rollType = 'roll'
  # if roll > sides / 4 * 3:
  #   rollType == 'goodroll'
  # if roll == sides:
  #   rollType = 'crit'
  # if roll < sides / 4:
  #   rollType = 'badroll'
  # if roll == 1:
  #   rollType = 'critmiss'

  html += "<p class={rollType}><img src='{media}/d{die}.png' class=dice alt='{die} sided die'> {roll}</p><p>{name} rolled {roll}.</p>".format(media=MEDIA_URL, die=sides, roll=roll, name=name, rollType=rollType)
  
  # sidessvg = '''<text x="100" y="220" style='font-size:200'>%d</text></svg>''' % roll 
  # html = html.replace('</svg>', sidessvg)
  # If this is not a GM private roll, log and share it.
  logMessage(message)
  if private == 'on':
    # Return the roll result to the GM, only.
    return "{record}<div class=rolled><p class={rollType}>Private roll: {roll}</p></div>".format(roll=html, record=record(), rollType=rollType)
  else:
# Record the last roll.
      f = open(LAST_ROLL, 'w')
      f.write(html)
      f.close()
  # return record()
  redirect(ROOT_URL)

# @route(APP_ROOT)
# @route(APP_ROOT + '/record')
# @route(APP_ROOT + '/roll')
# @route(APP_ROOT + '/say')
@route('/', method='ANY')
def record(history=False):
  '''Show the roll record.
  
  This is our main page render, every view uses this as it's return. 
  '''
  name = get_name() 
  rollLog = ROLL_LOG 

  # Provides permanent record, even after a clear.
  if history:
    rollLog = HISTORY_FILE 

# Open template
  f = open('roller.html', 'r')
  templateHTML = ' '.join(f.readlines())
  f.close()
  template = Template(templateHTML)
  
 # Open roll log 
  lines = readFile(rollLog)

  lines.reverse()
  record = '<li>'
  record += '</li><li>'.join(lines)
  record += '</li>'
 
# Open last roll image.
  lastroll = ''.join(readFile(LAST_ROLL))

  data = {}
  data['navigation'] = getNav()
  data['record'] = record 
  data['rolled']= lastroll
  data['name'] = name
  data['admin'] = getAdmin()
  data['app'] = ROOT_URL
  data['media'] = MEDIA_URL

  return template.safe_substitute(data) 

# Run me!
# if __name__ == '__main__':
#  bottle.debug(True)
#   bottle.run(host='localhost', port=8000)
# else:
application = bottle.default_app()

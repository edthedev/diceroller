" -------------------------------------------------------
"
"       EdTheDev's Minimal .vimrc 
"
"          Like a normal Vimrc, but with no plugins.
" -------------------------------------------------------
"
" When setting up on a new machine, remember to:
"  git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle"
"  vim +BundleInstall +qall

" How this Vimrc is organized
" The headings below are from the output of the Options command.
" :options
"
" http://dougireton.com/blog/2013/02/23/layout-your-vimrc-like-a-boss/

" =======================================================
"  1 important
" =======================================================
set nocompatible "Don't emulate old vi bugs.

" Let the leader key be ,  
let mapleader=","

" =======================================================
"  2 moving around, searching and patterns
" =======================================================

set ignorecase " Case insensitive search
set incsearch "Search as I type

" =======================================================
"  3 tags
" =======================================================
"
" Generate the .tags file.
map <Leader>gt :!ctags --file-scope=no --python-kinds=-iv -R -f .tags .
set tags=.tags;/

" =======================================================
"  4 displaying text
" =======================================================


set vb t_vb= "Flash instead of beep.
" Draw a nice line under the current line.
hi CursorLine guibg=#2d2d2d

" No SpellCheck by default.
set nospell
set ruler


set scrolloff=8      " Number of lines from vertical edge to start scrolling
" set sidescrolloff=15 " Number of cols from horizontal edge to start scrolling
" set sidescroll=1       " Number of cols to scroll at a time

set cursorline " Underline the line that the curson is on
hi CursorLine guibg=#2d2d2d

set ruler
" set textwidth=8000
set vb t_vb= "Flash instead of beep.


" Do not show line numbers
set nonumber

" Paste in text without too much trouble

set guifont=Monospace\ 12
" Do not add line breaks into newly entered text.
set wrapmargin=0
set textwidth=0

"

" =======================================================
"  5 syntax, highlighting and spelling
" =======================================================
syntax enable 

" Allow files to specify what their highlight mode is.
set modeline
" i.e.:
" # vi: syntax=apache


" Autocommands are commented out due to speed concerns:
"   I generally just use modeline instead for obscure types.
"
" Display .conf as INI                   
" au BufReadPost *.conf set ft=dosini

" Markdown
" au BufWinEnter *.{md,mdown,mkd,mkdn,markdown,mdwn}   set filetype=mkd

" Salt SLS files are YAML
" au BufWinEnter *.sls setlocal expandtab
" au BufWinEnter *.sls setlocal tabstop=2
" au BufWinEnter *.sls setlocal shiftwidth=2
" au BufWinEnter *.sls setlocal textwidth=0

" Ansible YML files are YAML
" au BufWinEnter *.yml setlocal expandtab
" au BufWinEnter *.yml setlocal tabstop=2
" au BufWinEnter *.yml setlocal shiftwidth=2
" au BufWinEnter *.yml setlocal textwidth=0

" =======================================================
"  6 multiple windows
" =======================================================

" Allow switching buffers without closing files.
set hidden

" Show open buffers
:map <Leader>b :buffers<Cr>

" Reminders:
"   :bn, :bc
"   :b <partial match for the buffer you want>
"   :badd <new file to open>

" =======================================================
"  7 multiple tab pages
" =======================================================
" =======================================================
"  8 terminal
" =======================================================
" =======================================================
"  9 using the mouse
" =======================================================
" =======================================================
" 10 printing
" =======================================================

" Print in black and white. SRSLY.
map <C-p> :color print_bw<CR>:hardcopy<CR>:color darkeclipse<CR>:syn on<CR>

" =======================================================
" 11 messages and info
" =======================================================
" =======================================================
" 12 selecting text
" =======================================================

" Re-select visual block after indent/outdent.
vnoremap < <gv
vnoremap > >gv

" =======================================================
" 13 editing text
" =======================================================
"
"
"Wiki Noformat tag.
ab nof {noformat}

"don't wrap in the default, utterly stupid way.
set lbr 

" Make backspace work like most other apps.
set backspace=indent,eol,start

"Parenthesis matching.
set showmatch 

" Gvim's default for paste is too many kepresses.
nmap <C-V> "+gP
imap <C-V> <ESC><C-V>i

" May as well support Ctrl+C as long as we're making changes.
vmap <C-C> "+y 

" =======================================================
" 14 tabs and indenting
" =======================================================
" Indentation is great.
set smartindent
" filetype indent on
filetype plugin indent on


" =======================================================
" 15 folding
" =======================================================
" =======================================================
" 16 diff mode
" =======================================================
" =======================================================
" 17 mapping
" =======================================================

" iPad and Android keyboards have no ESC Key
inoremap ii <esc>
" Tired of typing :Wq
cnoreabbrev W w

" =======================================================
" 18 reading and writing files
" =======================================================
" =======================================================
" 19 the swap file
" =======================================================
"   Do not leave backups in random locations. 
set backupdir=./.backup,/tmp
set directory=./.backup,/tmp

" =======================================================
" 20 command line editing
" =======================================================
" =======================================================
" 21 executing external commands
" =======================================================

" ex mode commands made easy
" nnoremap ; :

" :map <Leader>pc :!check-script %:p:h/%<Cr>
":map <Leader>pc :!hg ci%<Cr>
":map <Leader>pe :!tail /var/log/apache2/error.log
":map <Leader>ph :!check-script --html %<Cr>
":map <Leader>pm :!listmethods %<Cr>
" :map <Leader>pl :!pylib 

" Convert to HTML
map <leader>ch :!make html 

" Convert to PDF
map <leader>cp :!rst2pdf % > %:r.pdf;evince %:r.pdf &<Cr>

" Scratch Buffer
map <F7> :badd %:r-scratch.%:e
" Slides! 
map <leader>cs :!rst2slides % %:r.html;firefox %:r.html &<Cr>
" View Slides! 
" map <leader> :!firefox %:r.html &<Cr>

" Find word under cursor in entire project.
" map <Leader>f :!grep <C-r><C-w> . -R -A1 -B1 -n <bar> grep -v .svn <bar> vim -R -
map <Leader>f :!grep <C-r><C-w> . -R -A1 -B1 -n <bar> grep -v .svn <bar> grep -v tests <bar> vim -R -

" Diff current buffer vs saved file.
map <Leader>d :w !diff % -<Cr>

" Run!
map <F10> :!%

" map <F11> :!touch %:r.scratch.%:t <bar> :badd %:r.scratch.%:t<Cr>
map <Leader>os :badd %:r.scratch.%:t<Cr>

" :map <Leader>pr :!sudo /etc/init.d/apache2 restart<Cr>

" Sort lines!!
:map <Leader>ss :'<,'> !sort<Cr>

" -----------------------------------------------------------------
"    Wiki link stuff
" -----------------------------------------------------------------
map <Leader>ll :e <cfile><cr>
map <Leader>lw :e $HOME/Dropbox/notes<cr>
map <Leader>lh :e $HOME/Dropbox/notes<cr>
map <Leader>lb :buffers<Cr>
"map ,lj :bp<Cr>
"map ,lk :bn<Cr>

"      Send stuff to Wordpress
" -----------------------------------------------------------------
map <Leader>tw :!$HOME/.vim/bundle/vim-rst/bin/text-send-to-wordpress %<Cr>
" Sort lines!!
:map <Leader>ss :'<,'> !sort<Cr>

" map <leader>w :!nautilus ~/Dropbox/notes/wiki >/dev/null 2>&1<Cr>
map <leader>ow :!ranger ~/Dropbox/notes/wiki<Cr>
" map <leader>oh :!firefox http://vifm.sourceforge.net/docs.html &<Cr>
" map <leader>oh :!firefox http://vifm.sourceforge.net/docs.html &<Cr>
map <leader>ob :!ranger .<Cr>

" Copy entire document to clipboard.
map <leader>xc :!cat % <bar> xclip -sel c

" Shared Clipboard
set clipboard=unnamed


" =======================================================
" 22 running make and jumping to errors
" =======================================================
" =======================================================
" 23 language specific
" =======================================================
" =======================================================
" 24 multi-byte characters
" =======================================================
" =======================================================
" 25 various
" =======================================================

" =======================================================
"   Common Coding Commands
" =======================================================
" Keep the code PEP8 compliant.
" map <F2> :!pylint %:r.py > /tmp/pylint.txt <bar> :e /tmp/pylint.txt 
" map <F2> :e /tmp/pylint.txt <bar> :read !pylint %:r.py 

" Some generally useful settings
" -----------------------------------------------------------------------

" -------------------------
"
"	Vundle!!! Hooray!!!
"
" -------------------------

filetype off                   " required!
" filetype on " Enable file type detection

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" RST helper scripts.
Bundle 'edthedev/vim-rst'

" EdTheDev's Minion file organizer.
" Bundle 'edthedev/Minion'

" EdTheDev's Vim configuration for Python PEP8
Bundle 'edthedev/vim-pep8'

" EdTheDev's Vim configuration for TODO list items.
Bundle 'edthedev/vim-todo'

" Automatic closing of parenthesis
Bundle 'Raimondi/delimitMate'

" Use the Tab key for autocompletion, in insert mode.
" Bundle 'ervandew/supertab'

" Snippets utility
" Bundle 'SirVer/ultisnips'

" Browsing files made easier.
Bundle 'scrooloose/nerdtree'
nmap <leader>r :NERDTreeToggle<CR>

" Display tag (function/method) outline
Bundle 'majutsushi/tagbar'
nmap <leader>t :TagbarToggle<Cr>

filetype plugin indent on     " required!

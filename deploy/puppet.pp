group { "puppet":
ensure => "present",
}
	# Default file config
	File { owner => 0, group => 0, mode => 0644 }

	# Remind us which server we are on.
	file { '/etc/motd':
		 content => "Dice Roller Gunicorn Server"
	}

	file { '/home/vagrant/.vimrc':
		source=>'/vagrant/deploy/vimrc',
	}

	#exec { 'setup vim':
#		command=>'/vagrant/deploy/vim-bootstrap.sh',
#		require=>File['/home/vagrant/.vimrc'],
#	}

	# Update apt-get repositories 
	exec { 'sudo apt-get update':
		command=>'sudo apt-get update;touch /tmp/apt_updated',
		path=>'/usr/bin',
		creates=>'/tmp/apt_updated',
	}

	# Default Package Config
	Package { 
		provider=>'apt',
		require=>Exec['sudo apt-get update'],
	}

	package { 'apache':
		ensure=>purged,
	}

	package { 'apache2':
		ensure=>purged,
	}

	# Editing tools
	package { 'vim': }
	package { 'git': }

	# Django 1.6
	package { 'python': }
	package { 'python-pip': }
	package { 'django==1.6':
		provider=>'pip',
		require=>Package['python-pip'],
	 }

	# TastyPie for REST API
	package { 'django-tastypie':
		provider=>'pip',
		require=>Package['python-pip'],
	}

	# Gunicorn for HTTP/WSGI server.
	package { 'gunicorn': 
		provider=>'pip',
		require=>Package['python-pip'],
	}

	# Importd?  May use later.
	package { 'importd':
		provider=>'pip',
		require=>Package['python-pip'],
	}

	# Configure Nginx as a trivial proxy
	package { 'nginx-full': }
	file { "/etc/nginx/sites-enabled/default":
		ensure => link,
		require=>Package['nginx-full'],
		target => "/vagrant/deploy/nginx-site",
	}
	service { "nginx":
		require=>[Package['nginx-full'],
			File['/etc/nginx/sites-enabled/default'],
		],
		enable => true,
	}

from django.db import models
from tastypie.resources import ModelResource

import urlparse
from tastypie.serializers import Serializer

# Allows us to accept a wide variety of POST data formats.
class urlencodeSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'urlencode']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'urlencode': 'application/x-www-form-urlencoded',
        }
    def from_urlencode(self, data,options=None):
        """ handles basic formencoded url posts """
        qs = dict((k, v if len(v)>1 else v[0] )
            for k, v in urlparse.parse_qs(data).iteritems())
        return qs

    def to_urlencode(self,content): 
        pass

# Create your models here.
class Roll(models.Model):
    value = models.IntegerField(default=1)
    sides = models.IntegerField(default=20)
    name = models.CharField(max_length=250, default='Nobody')
# TODO: Bind a default random value for value in here, based on the value of sides...

class RollResource(ModelResource):
    class Meta:
        queryset = Roll.objects.all()
        resource_name = 'roll'

	# Accept any old POST format.
	serializer = urlencodeSerializer()
